<?php
//use Tuupola\Middleware\CorsMiddleware;
require __DIR__ . '/../vendor/autoload.php';
require '../src/config/db.php'; 
$app = new \Slim\App;
 


//cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Token');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('content-type: application/json; charset=utf-8');

// Route clients
require '../src/routes/routes.php';

$app->run();
