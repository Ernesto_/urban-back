<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app = new \Slim\App;

// Tabla Proveedores
// Get all data suppliers
$app->get('/api/suppliers', function(Request $request, Response $response){

    $query = "SELECT *  FROM proveedores";
    try
    {

        $db = new Connection();
        $db = $db->Connect();
        $result = $db->query($query);

        if($result->rowCount() > 0)
        {
            $suppliers = $result->fetchAll(PDO::FETCH_OBJ);

            // Construir el arreglo asociativo en el formato deseado
            $formattedData = [
                "data" => [],
                "meta" => [
                    "total_records" => $result->rowCount()
                ]
            ];

            foreach($suppliers as $supplier) {
                $formattedData["data"][] = [
                    "type" => "proveedor",
                    "id" => $supplier->proveedor,  // Ajusta según las columnas de tu tabla
                    "attributes" => [
                        "proveedor" => $supplier->proveedor,  // Ajusta según las columnas de tu tabla
                        "descripcion" => $supplier->descripcion,   // Ajusta según las columnas de tu tabla
                        "direccion" => $supplier->direccion ,
                        "contacto" => $supplier->contacto ,
                        "telefono" => $supplier->telefono ,
                        "ruc" => $supplier->ruc ,
                        "email" => $supplier->email
                         
                    ]
                ];
            }

            // Convertir el arreglo asociativo a JSON
            #echo json_encode($formattedData);
            return $response->withJson( $formattedData);
            
        } else {
            // No hay registros
            echo json_encode([
                "data" => [],
                "meta" => [
                    "total_records" => 0
                ]
            ]);
        }

        $result = null;
        $db = null;

    }
    catch(PDOException $e)
    {
        echo '{"error": { "text":'.$e->getMessage().'}';
    }
});

// Get data supplier by ID
$app->get('/api/suppliers/{id}', function(Request $request, Response $response){
    
    $id_client= $request->getAttribute('id');
    
    $query = "SELECT * FROM proveedores WHERE proveedor = $id_client";

    try
    {

        $db = new Connection();
        $db = $db->Connect();
        $result = $db->query($query);

        if($result->rowCount() > 0)
        {
            $suppliers = $result->fetchAll(PDO::FETCH_OBJ);

            // Construir el arreglo asociativo en el formato deseado
            $formattedData = [
                "data" => [],
             ];

            foreach($suppliers as $supplier) {
                $formattedData["data"][] = [
                    "type" => "proveedor",
                    "id" => $supplier->proveedor,  // Ajusta según las columnas de tu tabla
                    "attributes" => [
                        "proveedor" => $supplier->proveedor,  // Ajusta según las columnas de tu tabla
                        "descripcion" => $supplier->descripcion,   // Ajusta según las columnas de tu tabla
                        "direccion" => $supplier->direccion ,
                        "contacto" => $supplier->contacto ,
                        "telefono" => $supplier->telefono ,
                        "ruc" => $supplier->ruc ,
                        "email" => $supplier->email
                         
                    ]
                ];
            }

            // Convertir el arreglo asociativo a JSON
            #echo json_encode($formattedData);
            return $response->withJson( $formattedData);
            
        } else {
            // No hay registros
            echo json_encode([
                "data" => [],
             ]);
        }

        $result = null;
        $db = null;

    }
    catch(PDOException $e)
    {

        echo '{"error": { "text":'.$e->getMessage().'}';

    }

});
 
// POST Create new supplier
$app->post('/api/suppliers/new', function(Request $request, Response $response){
    
    // Obtener datos del cuerpo de la solicitud
    $data = $request->getParsedBody();

     // Imprimir contenido de $data en la consola del back
     //print_r($data);

    // Verificar si se proporcionaron los datos esperados
    if (!isset($data['descripcion'])  || !isset($data['ruc'])) {
        echo json_encode(["error" => "Faltan datos en el cuerpo de la solicitud"]);
        return;
    }

    // Obtener los valores de los datos
    $descripcion = $data['descripcion'];
    $ruc = $data['ruc'];

    // Consulta para verificar si ya existe el proveedor por el ruc
    $queryCheck = "SELECT * FROM proveedores WHERE ruc = :ruc";

    // Consulta para insertar un nuevo proveedor
    $queryInsert = "INSERT INTO proveedores( descripcion, ruc) VALUES ( :descripcion, :ruc)";
    
    try {
        $db = new Connection();
        $db = $db->Connect();

        // Verificar si el proveedor ya existe por el ruc
        $resultCheck = $db->prepare($queryCheck);
        $resultCheck->bindParam(':ruc', $ruc);
        $resultCheck->execute();

        if ($resultCheck->rowCount() > 0) {
            echo json_encode("El proveedor ya existe.");
        } else {
            // Insertar el nuevo proveedor
            $resultInsert = $db->prepare($queryInsert); 
            $resultInsert->bindParam(':descripcion', $descripcion);
            $resultInsert->bindParam(':ruc', $ruc);

            if ($resultInsert->execute()) {
                echo json_encode("Nuevo proveedor guardado.");
            } else {
                echo json_encode("No se ha podido guardar en la DB");
            }
        }

        $resultCheck = null;
        $resultInsert = null;
        $db = null;

    } catch(PDOException $e) {
        echo '{"error": { "text":'.$e->getMessage().'}';
    }

});
 
 // PUT Update supplier
$app->put('/api/suppliers/{id}', function(Request $request, Response $response) {
    try {
        // Obtener datos del cuerpo de la solicitud
        $data = $request->getParsedBody();
        $id_proveedor = $request->getAttribute('id');

        // Verificar si se proporcionó el cuerpo de la solicitud
        if (empty($data)) {
            echo json_encode(["error" => "Cuerpo de la solicitud vacío"]);
            return;
        }

        // Construir la consulta de actualización
        $updateQuery = "UPDATE proveedores SET ";
        $setClauses = [];

        foreach ($data as $key => $value) {
            // Excluir el campo "proveedor" de las columnas actualizadas
            if ($key !== 'proveedor') {
                $columnName = filter_var($key, FILTER_SANITIZE_STRING);
                $setClauses[] = "$columnName = :$columnName";
            }
        }

        $updateQuery .= implode(", ", $setClauses);
        $updateQuery .= " WHERE proveedor = :proveedor";

        // Imprimir la consulta para depuración
        echo "Update Query: $updateQuery\n";

        // Conexión a la base de datos
        $db = new Connection();
        $db = $db->Connect();

        // Preparar la consulta de actualización
        $updateStatement = $db->prepare($updateQuery);

        // Asignar valores de los datos para la actualización
        foreach ($data as $key => $value) {
            // Excluir el campo "proveedor" de los valores asignados
            if ($key !== 'proveedor') {
                $columnName = filter_var($key, FILTER_SANITIZE_STRING);
                $updateStatement->bindParam(":$columnName", $value);
            }
        }

        // Asignar valor para el WHERE
        $updateStatement->bindParam(':proveedor', $id_proveedor);

        // Ejecutar la consulta de actualización
        if ($updateStatement->execute()) {
            echo json_encode("Proveedor actualizado con éxito.");
        } else {
            echo json_encode("No se ha podido actualizar el proveedor en la DB");
        }

        // Limpiar
        $updateStatement = null;
        $db = null;

    } catch (PDOException $e) {
        echo '{"error": { "text":' . $e->getMessage() . '}';
    }
});


 
// DELETE proveedor
$app->delete('/api/suppliers/{id}', function(Request $request, Response $response){

    $id = $request->getAttribute('id');

    $query = "DELETE FROM proveedores WHERE proveedor = :id";
    
    try
    {

        $db = new Connection();
        $db = $db->Connect();
        $result = $db->prepare($query);

        $result->bindParam(':id', $id);
        $result->execute();
        if($result->rowCount() > 0)
        {

            echo json_encode("Proveedor borrado exitosamente.");

        }else
        {

            echo json_encode("No existe el proveedor con este ID");

        }

        $result = null;
        $db = null;

    }
    catch(PDOException $e)
    {

        echo '{"error": { "text":'.$e->getMessage().'}';

    }

});