<?php 


    class Connection{

        private $dbHost = "localhost";
        private $dbUser = "postgres"; 
        private $dbPass = "sistema";
        private $dbName = "postgres";

        //connection db

        public function Connect(){

            $mysqlConnect = "pgsql:host=$this->dbHost;dbname=$this->dbName";

            $dbConnection = new PDO($mysqlConnect, $this->dbUser, $this->dbPass);

            $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $dbConnection;

        }
        
    }